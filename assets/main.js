class Clock {
    constructor(template, $output)
    {
        this.template = template;
        this.$output = $output;
    }

    render()
    {
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        this.$output.html(this.template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs));
    }

    stop()
    {
        clearInterval(this.timer);
    }

    start()
    {
        this.render();
        this.timer = setInterval(() => this.render(), 1000);
    }
}

(function ()
{
    const utils = window.bpf.utils;
    window.bpf.config = $.extend(window.bpf.config, utils.getQuery(), true);

    axios.interceptors.response.use(undefined, function (error)
    {
        console.log('error', error);
        return Promise.reject(error);
    });

    function toggleWidgetContainerVisibility()
    {
        $('.widgetsContainer').toggleClass('widgetsContainer--hidden');
    }

    $(document).ready(function ()
    {
        const widgetWp = new WidgetWp({
            $root: $('.widgetWp'),
        });
        const widgetHa = new WidgetHa({
            $root: $('.widgetHa'),
        });
        const wallpaper = new Wallpaper();

        new Ha({
            widget: widgetHa
        });
        let photoprism = new Photoprism({
            onLoadedConfig: function ()
            {

            },
            widget: widgetWp,
            wallpaper: wallpaper,
        });

        $('.widgetCam').each(function ()
        {
            const $cam = $(this);
            new WidgetCam({
                $root: $cam,
                src: $cam.data('src')
            });
        });

        (function ()
        {
            let wasPlaying, seekTime;
            const player = wallpaper.videojs;
            const playerSeekStep = 5;

            $(document).keyup(function (e)
            {
                const mediaType = wallpaper.getMediaType();
                console.log('e.originalEvent.keyCode', e.originalEvent.keyCode);
                switch (e.originalEvent.keyCode)
                {
                    case 32://Space
                        e.preventDefault();
                        if (mediaType === 'video')
                        {
                            if (player.paused())
                            {
                                player.play();
                            } else
                            {
                                player.pause();
                            }
                        } else
                        {
                            if (photoprism.slideshowInterval)
                            {
                                photoprism.stopSlideshow();
                            } else
                            {
                                photoprism.startSlideshow(false);
                            }
                        }
                        break;
                    case 38://ArrowUp
                        e.preventDefault();
                        if (player.muted()) player.muted(false);
                        break;
                    case 40://ArrowDown
                        e.preventDefault();
                        if (!player.muted()) player.muted(true);
                        break;
                    case 70://f
                        e.preventDefault();
                        document.fullScreenElement && null !== document.fullScreenElement || !document.mozFullScreen && !document.webkitIsFullScreen ? document.documentElement.requestFullScreen ? document.documentElement.requestFullScreen() : document.documentElement.mozRequestFullScreen ? document.documentElement.mozRequestFullScreen() : document.documentElement.webkitRequestFullScreen && document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT) : document.cancelFullScreen ? document.cancelFullScreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitCancelFullScreen && document.webkitCancelFullScreen();
                        break;
                    case 37://ArrowLeft
                        e.preventDefault();
                        if (mediaType === 'video')
                        {
                            // Seek Backward
                            wasPlaying = !player.paused();
                            if (wasPlaying)
                            {
                                player.pause();
                            }
                            seekTime = player.currentTime() - playerSeekStep;
                            // The flash player tech will allow you to seek into negative
                            // numbers and break the seekbar, so try to prevent that.
                            if (seekTime <= 0)
                            {
                                seekTime = 0;
                            }
                            player.currentTime(seekTime);
                            if (wasPlaying)
                            {
                                player.play();
                            }
                        } else
                        {
                            photoprism.stopSlideshow();
                            photoprism.startSlideshow(true, photoprism.getPrevLoadedMedia());
                        }
                        break;
                    case 39://ArrowRight
                        e.preventDefault();
                        if (mediaType === 'video')
                        {
                            // Seek Forward
                            wasPlaying = !player.paused();
                            if (wasPlaying)
                            {
                                player.pause();
                            }
                            seekTime = player.currentTime() + playerSeekStep;
                            // Fixes the player not sending the end event if you
                            // try to seek past the duration on the seekbar.
                            let duration = player.duration();
                            if (seekTime >= duration)
                            {
                                seekTime = wasPlaying ? duration - .001 : duration;
                            }
                            player.currentTime(seekTime);
                            if (wasPlaying)
                            {
                                player.play();
                            }
                        } else
                        {
                            photoprism.stopSlideshow();
                            photoprism.startSlideshow(true);
                        }
                        break;
                    case 13://Enter
                        toggleWidgetContainerVisibility();
                        break;
                }
            });
        })();

        //
        // const clock = new Clock('h:m', $('.clock'));
        // clock.start();

        //Lock Screen
        //https://progressier.com/pwa-capabilities/screen-wake-lock#:~:text=The%20Screen%20Wake%20Lock%20API,while%20the%20app%20is%20running.

    });


})();


if ('serviceWorker' in navigator)
{
    navigator.serviceWorker.register('/sw.js')
        .then(function (registration)
        {
            console.log('Registration successful, scope is:', registration.scope);
        })
        .catch(function (error)
        {
            console.log('Service worker registration failed, error:', error);
        });
}