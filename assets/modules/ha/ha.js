class Ha {
    constructor(args)
    {
        const that = this;
        that.widget = args.widget;
        that.config = window.bpf.config.ha;

        //https://www.home-assistant.io/integrations/http/
        that.axios = axios.create({
            baseURL: that.config.baseUrl,
            timeout: 10000,
            headers: {
                'Authorization': `Bearer ${that.config.apiKey}`,
                'content-type': 'application/json',
            }
        });

        setInterval(function ()
        {
            that.updateSensors();
        }, 1000 * 20);
        that.updateSensors();
    }

    updateSensors()
    {
        const that = this;
        const requests = [];
        requests.push(that.axios.get('/api/states/sensor.outdoor_temperature'));
        requests.push(that.axios.get('/api/states/sensor.outdoor_humidity'));

        requests.push(that.axios.get('/api/states/sensor.bedroom_bme180_temperature'));
        requests.push(that.axios.get('/api/states/sensor.bedroom_bme680_humidity'));

        requests.push(that.axios.get('/api/states/sensor.cellar_humidity'));
        requests.push(that.axios.get('/api/states/sensor.cellar_temperature'));

        Promise
            .all(requests)
            .then(responses =>
            {
                that.widget.loadTemplates();
                for (let key in responses)
                {
                    let response = responses[key];
                    that.widget.updateSensor(response.data);
                }
            });
    }
}