class WidgetHa extends Widget {
    constructor(args)
    {
        super(args);
        const that = this;
    }

    escapeRegExp(string)
    {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }

    replaceAll(str, find, replace)
    {
        const that = this;
        return str.replace(new RegExp(that.escapeRegExp(find), 'g'), replace);
    }

    updateSensor(data)
    {
        const that = this;
        const $parametersValues = that.$root.find('.widget__parameterValue');
        $parametersValues.each(function ()
        {
            const $parameterValue = $(this);
            const template = $parameterValue.data('template');
            const search = `{{${data.entity_id}}}`;
            if (template.indexOf(search) === -1) return true;

            $parameterValue.html(that.replaceAll(
                $parameterValue.html(),
                search,
                `${data.state}${data.attributes.unit_of_measurement}`,
            ));
        });
    }
}