(function ()
{
    const utils = {};
    utils.getQuery = function (query)
    {
        if (query === undefined) query = location.search;
        return JSON.parse(JSON.stringify(Object.fromEntries(new URLSearchParams(query))));
    }
    window.bpf.utils = utils;
})();

