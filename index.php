<?php
$version = '0.2.1';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Photo Frame</title>
    <link rel="manifest" href="manifest.json">
</head>
<body>
<link rel="stylesheet" href="assets/vendor/reset.css?v=<?php echo $version ?>" type="text/css">
<link rel="stylesheet" href="assets/modules/widget/widget.css?v=<?php echo $version ?>" type="text/css">
<link rel="stylesheet" href="assets/vendor/videojs/video-js.min.css?v=<?php echo $version ?>" type="text/css">
<link rel="stylesheet" href="assets/style.css?v=<?php echo $version ?>" type="text/css">
<link rel="stylesheet" href="assets/modules/wallpaper/wallpaper.css?v=<?php echo $version ?>" type="text/css">
<link rel="stylesheet" href="assets/modules/widget-cam/widget-cam.css?v=<?php echo $version ?>" type="text/css">
<div class="wallpaper" data-media-type="image">
    <img src="assets/img/wallpaper.jpg">
    <video class="video-js vjs-default-skin" autoplay muted preload="auto" playsinline></video>
</div>
<!--<div class="clock"></div>-->
<div class="widgetsContainer widgetsContainer--right">
    <?php include_once 'right-widgets.php' ?>
</div>
<div class="widgetsContainer widgetsContainer--left">
    <?php include_once 'left-widgets.php' ?>
</div>
<script src="assets/vendor/axios.min.js?v=<?php echo $version ?>"></script>
<script src="assets/vendor/jquery-3.6.0.min.js?v=<?php echo $version ?>"></script>
<script src="assets/vendor/date.format.js?v=<?php echo $version ?>"></script>
<script src="assets/vendor/videojs/video.min.js?v=<?php echo $version ?>"></script>
<script src="config.js?v=<?php echo $version ?>"></script>
<script src="assets/utils.js?v=<?php echo $version ?>"></script>
<script src="assets/modules/widget/widget.js?v=<?php echo $version ?>"></script>
<script src="assets/modules/widget-wp/widget-wp.js?v=<?php echo $version ?>"></script>
<script src="assets/modules/wallpaper/wallpaper.js?v=<?php echo $version ?>"></script>
<script src="assets/modules/photoprism/photoprism.js?v=<?php echo $version ?>"></script>
<script src="assets/modules/widget-ha/widget-ha.js?v=<?php echo $version ?>"></script>
<script src="assets/modules/ha/ha.js?v=<?php echo $version ?>"></script>
<script src="assets/modules/widget-cam/widget-cam.js?v=<?php echo $version ?>"></script>
<script src="assets/main.js?v=<?php echo $version ?>"></script>
</body>
</html>