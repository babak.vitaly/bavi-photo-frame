Live photo frame that loads photos and videos from [Photoprism](https://www.photoprism.app/).

Features: 
* loading favorites photos and albums from [Photoprism](https://www.photoprism.app/);
* outside camera widgets;
* [Home Assistant](https://www.home-assistant.io/) sensors data in widgets;

![Demo](_other/demo-1.jpg "Demo")

URL options:
* **camUpdateInterval=1** //in seconds

Hotkeys:
* **enter** - hide\show widget
* **ArrowRight** - next\Seek Forward
* **ArrowLeft** - prev\Seek Backward
* **ArrowUp** - unmute
* **ArrowDown** - mute
* **F** - full screen mode