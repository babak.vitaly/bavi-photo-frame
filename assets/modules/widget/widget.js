class Widget {
    constructor(args)
    {
        const that = this;
        that.$root = args.$root;
        that.$title = that.$root.find('.widget__title');
        that.$body = that.$root.find('.widget__body');
    }

    getParameterById(id)
    {
        const that = this;
        return that.$root.find(`.widget__parameter[data-id="${id}"]`)
    }

    getParameterValueById(id)
    {
        const that = this;
        return that.$root.find(`.widget__parameter[data-id="${id}"] .widget__parameterValue`)
    }

    setParameterValue(id, value)
    {
        const that = this;
        that.getParameterValueById(id).html(value);
    }

    loadTemplates()
    {
        const that = this;
        that.$root.find('.widget__parameterValue').each(function ()
        {
            const $parameterValue = $(this);
            $parameterValue.html($parameterValue.data('template'));
        });
    }
}