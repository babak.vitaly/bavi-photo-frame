class Wallpaper {
    constructor()
    {
        const that = this;
        that.$root = $('.wallpaper');
        that.$image = that.$root.find('>img');
        that.mediaType = null;

        const options = {
            controls: true,
            autoplay: true,
            preload: 'auto',
            // fluid: true,
            fill: true,
            controlBar: {
                fullscreenToggle: true,
            }
        };
        that.videojs = videojs(that.$root.find('>video').get(0), options);

        // that.videojs.on('canplay', function ()
        // {
        //     console.log('canplay');
        //     that.videojs.play();
        // });

        // $(document).keyup(function (event) {
        //     alert('bar');
        // });
    }

    getMediaType()
    {
        const that = this;
        return that.mediaType;
    }

    setMediaType(type)
    {
        const that = this;
        that.mediaType = type;
        that.$root.attr('data-media-type', type);
    }

    setImage(src)
    {
        const that = this;
        that.setMediaType('image');
        that.$image.attr('src', src);
        console.log('setImage', src);
    }

    setVideo(videoSrc, posterSrc = null)
    {
        const that = this;
        that.setMediaType('video');
        that.videojs.src({
            type: "video/mp4",
            src: videoSrc
        });
        that.videojs.poster(posterSrc);
    }
}