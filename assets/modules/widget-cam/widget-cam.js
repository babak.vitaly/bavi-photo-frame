class WidgetCam extends Widget {
    constructor(args)
    {
        super(args);
        const that = this;
        that.$img = that.$root.find('img');
        that.loading = false;
        that.src = args.src;
        that.update();

        setInterval(function ()
        {
            that.update();
        }, 1000 * window.bpf.config.camUpdateInterval);

        that.$img.on("load", function ()
        {
            that.loading = false;
        });
        that.$img.on('error', function ()
        {
            that.loading = false;
        });
        that.$img.click(function ()
        {
            that.update();
        });
    }

    update()
    {
        const that = this;
        if (that.loading)
        {
            console.log('image is loading...');
            return;
        }
        var srcParts = that.src.split('?');
        var query = that.getQuery(srcParts[1]);
        query.v = Date.now() + Math.random();
        that.$img.attr('src', srcParts[0] + '?' + $.param(query));
        that.loading = true;
    }

    getQuery(query)
    {
        var result = {};
        query.split("&").forEach(function (part)
        {
            if (!part) return;
            part = part.split("+").join(" "); // replace every + with space, regexp-free version
            var eq = part.indexOf("=");
            var key = eq > -1 ? part.substr(0, eq) : part;
            var val = eq > -1 ? decodeURIComponent(part.substr(eq + 1)) : "";
            var from = key.indexOf("[");
            if (from == -1) result[decodeURIComponent(key)] = val;
            else
            {
                var to = key.indexOf("]", from);
                var index = decodeURIComponent(key.substring(from + 1, to));
                key = decodeURIComponent(key.substring(0, from));
                if (!result[key]) result[key] = [];
                if (!index) result[key].push(val);
                else result[key][index] = val;
            }
        });
        return result;
    }
}