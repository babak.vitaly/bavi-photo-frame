class Photoprism {
    constructor(args)
    {
        const that = this;
        that.images = [];
        that.albums = [];
        that.albumQuery = '';
        that.nextImageIndex = 0;
        that.wallpaper = args.wallpaper;
        that.widget = args.widget;
        that.slideshowInterval = null;
        that.emptyLoadImageResultCounter = 0;
        that.globalConfig = window.bpf.config.photoprism;

        //https://pkg.go.dev/github.com/photoprism/photoprism/internal/api?utm_source=godoc#GetPhoto
        that.axios = axios.create({
            baseURL: that.globalConfig.baseUrl,
            timeout: 10000,
            //headers: {'X-Session-ID': '26ec8361479f5d08eadff190afd5000b9beb742e060e0574'}//it's temporary
        });
        that.login(function (response)
        {
            that.config = response.config;
            that.axios.defaults.headers['X-Session-ID'] = response.id;
            args.onLoadedConfig();

            let stayResponses = 2;
            // console.log('response', response);
            // console.log('that.axios', that.axios);
            // return;


            function receivedResponse()
            {
                stayResponses--;
                if (stayResponses <= 0)
                {

                }
            }

            that.loadAlbums(function (albums)
            {
                console.log('albums', albums);
                that.albums = albums;
                // that.albumQuery = that.prepareAlbumQuery(albums);
                that.loadImages(function ()
                {
                    that.startSlideshow(true);
                });
            });


            // that.loadAlbums(function (folders)
            // {
            //     console.log('folders', folders);
            //     that.folders = folders;
            //     that.folderQuery = that.prepareFolderQuery(that.folders);
            //     receivedResponse();
            // }, 'folder');
        });

        that.wallpaper.videojs.on('ended', function ()
        {
            console.log('ended');
            that.startSlideshow();
        });

        that.wallpaper.videojs.on('error', function ()
        {
            console.log(that.wallpaper.videojs.error());
            that.startSlideshow();
        });
    }


    //fit_1280
    getImageSrc(image, size = 'fit_2048')
    {
        const that = this;
        return `${that.globalConfig.baseUrl}api/v1/t/${image.Hash}/${that.config.previewToken}/${size}`;
    }

    getVideoSrc(image)
    {
        const that = this;
        return `${that.globalConfig.baseUrl}api/v1/videos/${image.Files[1]['Hash']}/${that.config.previewToken}/avc`;
    }

    loadConfig(cb)
    {
        const that = this;
        return that.axios.get(
            '/api/v1/config', {
                params: {
                    merged: true,
                    count: 1,
                    public: true
                }
            }
        ).then(function (response)
        {
            cb(response.data);
        });
    }

    login(cb)
    {
        const that = this;
        return that.axios.post(
            '/api/v1/session', {
                username: that.globalConfig.username,
                password: that.globalConfig.password,
            }
        ).then(function (response)
        {
            cb(response.data);
        });
    }


    loadAlbums(cb)
    {
        const that = this;
        return that.axios.get(
            '/api/v1/albums', {
                params: {
                    count: 2000,
                    //type: type,
                    order: 'date',
                }
            }
        ).then(function (response)
        {
            const albums = [];
            for (let key in response.data)
            {
                let album = response.data[key];
                if (!album.Favorite) continue;

                albums.push(album);
            }
            cb(albums);
        });
    }

    prepareAlbumQuery(albums)
    {
        const that = this;
        const parts = [];
        for (let key in albums)
        {
            let album = albums[key];
            parts.push(`"${album.UID}"`);
        }
        return parts.join(', ');
    }

    prepareFolderQuery(albums)
    {
        const that = this;
        const parts = [];
        for (let key in albums)
        {
            let album = albums[key];
            parts.push(album.Path);
        }
        return parts.join('|');
    }

    getType(media)
    {
        //Sometimes there is no video file for the video, like here - CellID: "s2:40c5ca2d6ca4"
        if (media.Type === 'video' && media.Files[1] !== undefined) return 'video';
        return "image";
    }

    getDisplayedUids()
    {
        let displayedUids = localStorage.displayedUids;
        if (displayedUids !== undefined) displayedUids = JSON.parse(displayedUids);
        else displayedUids = [];
        return displayedUids;
    }

    setDisplayedUids(uids)
    {
        localStorage.displayedUids = JSON.stringify(uids);
    }

    //Might be a video
    updateMedia(media)
    {
        const that = this;
        console.log('updateMedia:media', media);
        if (media === undefined)
        {
            console.error('updateMedia:media === undefined');
            return;
        }

        /**
         * @type WidgetCam
         */
        const widget = that.widget;
        if (that.getType(media) === 'video')
        {
            that.stopSlideshow();
            that.wallpaper.setVideo(that.getVideoSrc(media), that.getImageSrc(media));
        } else
        {
            that.wallpaper.setImage(that.getImageSrc(media));
        }

        widget.getParameterValueById('date').html(dateFormat(media.TakenAtLocal, "d mmmm yyyy H:MM"));
        widget.getParameterValueById('device').html(media.CameraModel);
        widget.getParameterValueById('filename').html(media.FileName);

        widget.getParameterById('album').css('display', 'none');
        widget.getParameterValueById('album').html('');

        widget.getParameterById('place').css('display', media.PlaceCity ? '' : 'none');
        widget.getParameterValueById('place').html(media.PlaceCity);

        const displayedUids = that.getDisplayedUids();
        displayedUids.push(media.FileUID);
        that.setDisplayedUids(displayedUids);
    }

    getRandomInt(min, max)
    {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    getNextMedia()
    {
        const that = this;
        const image = that.images[that.nextImageIndex];
        that.nextImageIndex++;
        console.log('that.nextImageIndex', that.nextImageIndex);

        if (that.nextImageIndex > that.images.length - 1)
        {
            that.stopSlideshow();
            that.loadImages(function ()
            {
                that.nextImageIndex = 0;
                console.log('asadasdasd1223123123')
                that.startSlideshow(false);
            });
        }
        return image;
    }

    getPrevLoadedMedia()
    {
        const that = this;
        if (that.nextImageIndex > 0) that.nextImageIndex--;
        return that.images[that.nextImageIndex - 1];
    }

    stopSlideshow()
    {
        const that = this;
        clearInterval(that.slideshowInterval);
        that.slideshowInterval = null;
    }

    startSlideshow(immediately = true, media)
    {
        const that = this;
        if (that.slideshowInterval) return;

        that.slideshowInterval = setInterval(function ()
        {
            that.updateMedia(media === undefined ? that.getNextMedia() : media);
        }, 1000 * 5);
        if (immediately)
        {
            that.updateMedia(media === undefined ? that.getNextMedia() : media);
        }
    }

    getRandomDate(date1, date2)
    {
        function randomValueBetween(min, max)
        {
            return Math.random() * (max - min) + min;
        }

        var date1 = date1 || '01-01-1970';
        var date2 = date2 || new Date().toLocaleDateString();
        date1 = new Date(date1).getTime()
        date2 = new Date(date2).getTime()
        if (date1 > date2)
        {
            return new Date(randomValueBetween(date2, date1));
        } else
        {
            return new Date(randomValueBetween(date1, date2));
        }
    }

    getQueryDateAfter()
    {
        const that = this;
        const dateCurrent = new Date();
        const dateAfter = that.getRandomDate(new Date('2000-05-01T00:00:00'), dateCurrent);
        const diffTime = Math.abs(dateCurrent - dateAfter);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        if (diffDays < 600) dateAfter.setFullYear(dateAfter.getFullYear() - 2);
        return dateAfter;
    }

    formatQueryDate(date)
    {
        return date.toISOString().split('T')[0];
    }

    loadImages(cb)
    {
        const that = this;
        // https://docs.photoprism.app/user-guide/organize/search/
        // q:         string Query string

        // label:     string Label
        // cat:       string Category
        // country:   string Country code
        // camera:    int    UpdateCamera ID
        // order:     string Sort order
        // count:     int    Max result count (required)
        // offset:    int    Result offset
        // before:    date   Find photos taken before (format: "2006-01-02")
        // after:     date   Find photos taken after (format: "2006-01-02")
        // favorite:  bool   Find favorites only


        console.log('loadImages');
        let dateAfter = that.formatQueryDate(that.getQueryDateAfter());
        console.log('dateAfter', dateAfter);

        // const qChronic = that.axios.get('/api/v1/photos', {
        //     params: {
        //         q: 'filename:"Отсортированные/Хроника/*"',
        //         merged: true,//Adds Files information
        //         count: 10,
        //         after: dateAfter,
        //         public: true,
        //         order: 'oldest',
        //         offset: that.getRandomInt(0, 1000),
        //     }
        // });

        const qFavorite = that.axios.get('/api/v1/photos', {
            params: {
                merged: true,
                count: 25,
                favorite: true,
                after: dateAfter,
                public: true,
                order: that.getRandomInt(0, 1) ? 'oldest' : 'newest',
                offset: that.getRandomInt(0, 100),
            }
        });

        const album = that.albums[that.getRandomInt(0, that.albums.length - 1)];
        const qAlbumsParams = {
            merged: true,
            count: 30,
            public: true,
            order: 'oldest',
            //  offset: that.getRandomInt(0, 100),
        }
        if (album.Type === 'folder')
        {
            qAlbumsParams.q = `path:"${album.Path}"`;
        } else
        {
            qAlbumsParams.album = album.UID;
        }
        console.log('qAlbumsParams', qAlbumsParams);
        const qAlbums = that.axios.get('/api/v1/photos', {
            params: qAlbumsParams,
        });

        Promise
            .all([
                // qChronic,
                qFavorite,
                qAlbums,
            ])
            .then(responses =>
            {
                // const rChronic = responses[0]
                const rFavorite = responses[0];
                const rAlbums = responses[1];
                // console.log('rChronic', rChronic.data);
                console.log('rFavorite', rFavorite.data);
                console.log('rAlbums', rAlbums.data);

                let images = [];

                // for (let key in rChronic.data)
                // {
                //     let image = rChronic.data[key];
                //     if (image.Path.indexOf('Прочие') !== -1) continue;
                //
                //     images.push(image);
                // }

                images = images.concat(rFavorite.data);
                images = images.concat(rAlbums.data);


                const addedUids = [];
                that.images = [];
                const displayedUids = that.getDisplayedUids();
                for (let key in images)
                {
                    let image = images[key];
                    //Skip already added images
                    if ($.inArray(image.FileUID, addedUids) !== -1) continue;

                    //Skip displayed uids
                    if ($.inArray(image.FileUID, displayedUids) !== -1) continue;

                    that.images.push(image);
                    addedUids.push(image.FileUID);
                }

                that.images.sort(function (a, b)
                {
                    let dateA = 0;
                    let dateB = 0;
                    if (a.TakenAtLocal)
                    {
                        dateA = (new Date(a.TakenAtLocal)).getTime();
                    }
                    if (b.TakenAtLocal)
                    {
                        dateB = (new Date(b.TakenAtLocal)).getTime();
                    }
                    return dateA - dateB;
                });

                console.log('that.images', that.images);

                if (that.images.length === 0)
                {
                    that.emptyLoadImageResultCounter++;
                    setTimeout(function ()
                    {
                        that.loadImages(function ()
                        {
                            cb(that.images);
                        });
                    }, 300);
                    if (that.emptyLoadImageResultCounter > 10)
                    {
                        that.setDisplayedUids([]);
                    }
                } else that.emptyLoadImageResultCounter = 0;

                //that.nextImageIndex = that.getRandomInt(0, Object.keys(that.images).length - 1);
                cb(that.images);
            });
    }

}